﻿using Orbus.AdventureWorks.DataAccess;
using Orbus.AdventureWorks.DataAccess.Entity;
using Services.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Services
{
    [AuthorizeUser]
    public class ProductApiController : ApiController
    {
        private AdventureWorksContext _dbContext;
        public ProductApiController()
        {
            _dbContext = new Orbus.AdventureWorks.DataAccess.AdventureWorksContext(ConfigurationManager.ConnectionStrings["OrbusConnectionString"].ConnectionString);
        }

        
        public async Task<List<Product>> Get()
        {
            return await _dbContext.ProductRepository.GetProducts();
        }

        // GET api/<controller>/5
        public async Task<Product> Get(int id)
        {
            return await _dbContext.ProductRepository.GetProductById(id);
        }

        // POST api/<controller>
        public async void Post([FromBody]Product model)
        {
            await _dbContext.ProductRepository.CreateProduct(model);
        }

        // PUT api/<controller>/5
        public async void Put(int id, [FromBody]Product model)
        {
            await _dbContext.ProductRepository.UpdateProduct(model);
        }

        // DELETE api/<controller>/5
        public async void Delete(int id)
        {
            await _dbContext.ProductRepository.DeleteProduct(id);
        }
    }
}