﻿using Orbus.AdventureWorks.DataAccess;
using Orbus.AdventureWorks.DataAccess.Entity;
using Services.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Services.Controller
{
    [AuthorizeUser]
    public class ProductCategoryApiController : ApiController
    {
        private AdventureWorksContext _dbContext;
        public ProductCategoryApiController()
        {
            _dbContext = new Orbus.AdventureWorks.DataAccess.AdventureWorksContext(ConfigurationManager.ConnectionStrings["OrbusConnectionString"].ConnectionString);
        }
        // GET api/<controller>
        public async Task<List<ProductCategory>> Get()
        {
            return await _dbContext.ProductCategoryRepository.GetProductCategories(); 
        }

        // GET api/<controller>/5
        public async Task<ProductCategory> Get(int id)
        {
            return await _dbContext.ProductCategoryRepository.GetProductCategoryById(id);
        }
    }
}