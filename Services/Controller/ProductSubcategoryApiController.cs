﻿using Orbus.AdventureWorks.DataAccess;
using Orbus.AdventureWorks.DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Services.Controller
{
    public class ProductSubcategoryApiController : ApiController
    {
        private AdventureWorksContext _dbContext;
        public ProductSubcategoryApiController()
        {
            _dbContext = new Orbus.AdventureWorks.DataAccess.AdventureWorksContext(ConfigurationManager.ConnectionStrings["OrbusConnectionString"].ConnectionString);
        }
        // GET api/<controller>
        public async Task<List<ProductSubcategory>> Get()
        {
            return await _dbContext.ProductSubCategoryRepository.GetProductSubcategories();
        }

        // GET api/<controller>/5
        public async Task<List<ProductSubcategory>> Get(int id)
        {
            return await _dbContext.ProductSubCategoryRepository.GetProductSubcategoriesByProductCategoryId(id);
        }

    }
}