﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Services.Utility
{
    public class AuthorizeUser:AuthorizeAttribute
    {
        public AuthorizeUser()
        {

        }
        protected override bool IsAuthorized(HttpActionContext httpContext)
        {
            var headers = httpContext.Request.Headers;

            if (headers.Contains("saASFe982dq19Dskadj"))
            {
                return true;
            }
            return false;
        }
    }
}