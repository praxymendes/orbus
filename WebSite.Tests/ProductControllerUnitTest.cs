﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;
using Services.Controller;
using System.Threading.Tasks;
using System.Collections.Generic;
using Orbus.AdventureWorks.DataAccess.Entity;

namespace WebSite.Tests
{
    [TestClass]
    public class ProductControllerUnitTest
    {
        private ProductApiController _apiController;
        private ProductCategoryApiController _categoryApiController;
        private ProductSubcategoryApiController _subCategoryApiController;
        public ProductControllerUnitTest()
        {
            _apiController = new ProductApiController();
            _categoryApiController = new ProductCategoryApiController();
            _subCategoryApiController = new ProductSubcategoryApiController();
        }
        [TestMethod]
        public void Can_Get_details()
        {
            var lstDetails = Task.Run(() =>_apiController.Get().Result);

            Assert.IsTrue((lstDetails.Result).Count > 0, "Passed");
        }

        [TestMethod]
        public void Can_Create_New()
        {

            Task.Run(() => _apiController.Post(GetModel()));
            var lstDetails = Task.Run(() =>  _apiController.Get().Result.Find(x => x.Key == "test"));
            Assert.IsTrue(lstDetails != null, "Passed");
        }

        private Product GetModel()
        {
            return new Product
            {
                Key = "test",
                Name = "test",
                Price = 12,
                ProductSubcategoryId = 1,
                StockLevel = 123
            };
        }
    }
}
