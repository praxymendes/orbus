﻿using Orbus.AdventureWorks.DataAccess.Entity;
using Services;
using Services.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class ProductController : Controller
    {
        private ProductApiController _apiController;
        private ProductCategoryApiController _categoryApiController;
        private ProductSubcategoryApiController _subCategoryApiController;
        public ProductController()
        {
            _apiController = new ProductApiController();
            _categoryApiController = new ProductCategoryApiController();
            _subCategoryApiController = new ProductSubcategoryApiController();
        }
        //
        // GET: /Product/

        public async Task<ActionResult> Index(FormCollection form)
        {
            var productCategoryId = 0;
            var productSubcategoryId = 0;

            //Check for selected product category
            if (!string.IsNullOrWhiteSpace(form["Id"]))
            {
                productCategoryId = Convert.ToInt32(form["Id"]);
            }

            //check for selected product sub category
            if (!string.IsNullOrWhiteSpace(form["Key"]))
            {
                productSubcategoryId = Convert.ToInt32(form["Key"]);
            }

            //call web api and get products, product category n product sub category
            var result = await _apiController.Get();
            var category = await _categoryApiController.Get();
            var subCategory = await _subCategoryApiController.Get();

            //filter
            if (productCategoryId > 0)
            {
                subCategory = subCategory.Where(x => x.ProductCategoryId == productCategoryId).ToList();
            }
            if (productSubcategoryId > 0)
            {
                result = result.Where(x => x.ProductSubcategoryId == productSubcategoryId).ToList();
            }

            //data to bind to drop down list
            SelectList listSubCategory = new SelectList(subCategory, "Id", "Name", productSubcategoryId);
            SelectList listCategory = new SelectList(category, "Id", "Name", productCategoryId);

            ViewBag.ProductCategory = listCategory;
            ViewBag.ProductSubcategory = listSubCategory;
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Product model)
        {
            await Task.Run(() => _apiController.Post(model));
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (!id.HasValue)
                return HttpNotFound();
            var lstDetails = await _apiController.Get(id.Value);
            return View("Edit", lstDetails);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Product model)
        {
           
            await Task.Run(() =>  _apiController.Put(model.Id, model));
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Delete(int id)
        {
            await Task.Run(() => _apiController.Delete(id));
            return RedirectToAction("Index");
        }
       
    }
}
