﻿using Orbus.AdventureWorks.DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    public class SearchViewModel
    {
        public List<Product> Products { get; set; }
        public List<ProductCategory> ProductCategory { get; set; }
        public List<ProductSubcategory> ProductSubcategory { get; set; }
    }
}